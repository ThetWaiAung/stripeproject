<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('subscriptions', 'SubscriptionController@store');
Route::delete('subscriptions', 'SubscriptionController@destroy')->name('subscription.delete');
Route::post('subscriptions/{id}', 'SubscriptionController@update')->name('subscription.update');

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::post('/stripe/webhook', 'WebhookController@handle');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/plans', 'PlanController@index')->name('plans.index');
    Route::get('/plan/{plan}', 'PlanController@show')->name('plans.show');
    Route::get('/subscribe', function () {
        $plans = App\Plan::all();
        return view('subscribe', compact('plans'));
    });

    Route::post('/subscribe_process', 'CheckoutController@subscribe_process')->name('subscribe_process');
    Route::post('/charge', 'CheckoutController@charge');

    // Route::post('/subscription', 'SubscriptionController@create')->name('subscription.create');
    // Route::post('/subscription', 'SubscriptionController@store')->name('subscription.store');

    // Route::get('stripe', 'StripePaymentController@stripe');
    // Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');
});
