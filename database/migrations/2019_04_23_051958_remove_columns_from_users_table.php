<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsFromUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('stripe_id')->after('email')->nullable();
            $table->boolean('stripe_active')->default(false);
            $table->boolean('stripe_plan')->nullable();
            $table->string('stripe_subscription')->nullable();
            $table->timestamp('subscription_end_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['stripe_id','stripe_active','subscription_end_at']);
        });
    }
}
