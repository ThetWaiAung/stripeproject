<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name'=>'My book',
            'description'=>'A great book',
            'price'=>2000,
            'created_at'=> \Carbon\Carbon::now(),
            'updated_at'=> \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'name'=>'My Great book',
            'description'=>'A really great book',
            'price'=>3000,
            'created_at'=> \Carbon\Carbon::now(),
            'updated_at'=> \Carbon\Carbon::now()
        ]);
    }
}
