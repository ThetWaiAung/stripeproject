<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Subscription;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubscriptionTest extends TestCase
{
    /** @test */
    use DatabaseTransactions, \InteractsWithStripe;
    public function testSubscribesUser()
    {
        // Given I have a registered user who is not  subscribed.
        // $user = factory('App\User')->create(['stripe_active' => false]);
        // When I create a subscription for that user.

        // $subscription = $user->subscription();
        // $subscription->create($this->getPlan(), $this->getStripeToken());
        $user = $this->makeSubscribedUser(['stripe_active' => false]);
        //Then, they should have a recurring subscription with Stripe.
        // and should be active in our system
        $user = $user->fresh();
        $this->assertTrue($user->isSubscribed());
        try {
            $data = $user->subscription()->retrieveStripeSubscription();
        } catch (Exception $e) {
            $this->fail('Excepted to see a Stripe subscription, but did not.');
        }
    }
    /**
     * @test
     */
    public function testItSubscribesUserUsingCouponCode()
    {
        $user = factory('App\User')->create();
        // When I create a subscription for that user.
        $user->subscription()->usingCoupon('TEST-COUPON')->create($this->getPlan(), $this->getStripeToken());
        // Fetch user's Stripe customer object
        // And then maker sure that their last invoice includes the coupon discount.
        $customer = $user->subscription()->retrieveStripeCustomer();
        // dd($customer->invoices());
        try {
            $couponThatWasAppliedToStripe = $customer->invoices()->data[0]->discount->coupon->id;

            $this->assertEquals('TEST-COUPON', $couponThatWasAppliedToStripe);
        } catch (Exception $e){
            $this->fail('Excepted a coupon to be applied to the Stripe cutsomer, but did not find one.');
        }
    }

    /**
     * @test
     */
    public function testItCancelsUsersSubscription()
    {
        //Given we have a subscrib
        $user = $this->makeSubscribedUser();

        $user->subscription()->cancel();

        $stripeSubscription = $user->subscription()->retrieveStripeSubscription();
        // dd($stripeSubscription);

        $this->assertNotNull($stripeSubscription->canceled_at);//Stripe\Subscription->canceled_at could not be null

        $this->assertFalse($user->isSubscribed()); //subsctiption value could be false

        $this->assertNotNull($user->subscription_end_at); //subscription_end_at could not be null
    }
    protected function makeSubscribedUser($overrides = [])
    {
        $user = factory('App\User')->create($overrides);
        // When I create a subscription for that user.
        $user->subscription()->create($this->getPlan(), $this->getStripeToken());

        return $user;
    }
}
