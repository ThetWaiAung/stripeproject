<?php

namespace Tests\Feature;

use Tests\TestCase;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BillableTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * @test
     */
    public function testItDeterminesIfUserSubscriptionIsActive()
    {
        $user = factory('App\User')->create([
            'stripe_active' => true,
            'subscription_end_at' => null
        ]);
        $this->assertTrue($user->isActive());
        // If they've canceled, but are on their grace period, they're still active.
        $user->update([
            'stripe_active' => false,
            'subscription_end_at' => Carbon::now()->addDays(2)
        ]);

        $this->assertTrue($user->isActive());
        // If they've canceled, and are over their grace period, they aren't active.
        $user->update([
            'stripe_active' => false,
            'subscription_end_at' => Carbon::now()->subDays(2)
        ]);
        $this->assertFalse($user->isActive());
    }
    /**
     * @test
     */
    function testItDeterminesIfUsersSubscriptionIsOnAGracePeriod()
    {
        // Subscribed users are not on their grace period.
        $user = factory('App\User')->create([
            'subscription_end_at' => null
        ]);
        $this->assertFalse($user->isOnGracePeriod());

        // Those who have recently canceled will be on a grace period.
        $user->subscription_end_at = Carbon::now()->addDays(2);
        $this->assertTrue($user->isOnGracePeriod());

        // Those over their grace period are not it.
        $user->subscription_end_at = Carbon::now()->subDays(2);
        $this->assertFalse($user->isOnGracePeriod());
    }
}
