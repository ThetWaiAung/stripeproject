<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Http\Controllers\WebhookController;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WebhooksControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use DatabaseTransactions;

    public function testConvertStripeToName()
    {
        $name = (new WebhookController)->eventToMethod('customer.subscription.deleted');

        $this->assertEquals('whenCustomerSubscriptionDeleted', $name);
    }

    public function testDeactivatesUsersSubscriptionAndDeletedStripeEnd()
    {
        $user = factory('App\User')->create([
            'stripe_active' => 1,
            'stripe_id' => 'fake_stripe_id'
        ]);
      // When I trigger the WebhookController with the customer.subscription.deleted event,
        $this->post('stripe/webhook', [
            'type' => 'customer.subscription.deleted',
            'data' => [
                'object' => [
                    'customer' => $user->stripe_id
                ]
            ]
        ]);
        // Then, the user should be deactivated.
        $this->assertFalse($user->fresh()->isSubscribed());
    }
}
