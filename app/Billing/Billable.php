<?php

namespace App\Billing;

use Carbon\Carbon;
use App\Subscription;
use App\Billing\Payment;

trait Billable
{
    public static function byStripeId($stripeId)
    {
        return static::where('stripe_id', $stripeId)->first();
    }
    /**
     * Activate ths user's subscription.
     *
     * @param string|null $customerId
     * @param string|null $subscriptionId
     * @return bool
     */
    public function activate($customerId = null, $subscriptionId = null)
    {
        return $this->forceFill([
            'stripe_id' => $customerId ?? $this->stripe_id,
            'stripe_active' => true,
            'stripe_subscription' => $subscriptionId ?? $this->stripe_subscription,
            'subscription_end_at' => null
        ])->save();
    }
    /**
     * Deactivate the user's subscription.
     *
     * @param mixed $endDate
     * @return bool
     */

    public function deactivate($endDate = null)
    {
        $endDate = $endDate ?: \Carbon\Carbon::now(); //Default is Carbon\Carbon::now()
        return $this->forceFill([
            'stripe_active' => false,
            'subscription_end_at' => $endDate
        ])->save();
    }

    public function subscription()
    {
        return new Subscription($this);
    }
    /**
     * Determine if the user is subscribed.
     *
     * @return boolean
     */
    public function isSubscribed()
    {
        return !!$this->stripe_active;
    }

    public function isActive()
    {
        return is_null($this->subscription_end_at) || $this->isOnGracePeriod();
    }

    public function isOnGracePeriod()
    {
        if(!$endsAt = $this->subscription_end_at)
        {
           return false;
        }
        return Carbon::now()->lt(Carbon::instance($endsAt));
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
    public function getSubscriptionEndAttribute()
    {
        return Carbon::parse($this->subscription_end_at)->format('d-m-Y');
    }
}
