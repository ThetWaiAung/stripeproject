<?php

namespace App\Http\Controllers;

use App\User;
use App\Billing\Billable;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    use Billable;
    public function handle()
    {

        // $type = $request->json('type');

        $payload = request()->all();

        $method = $this->eventToMethod($payload['type']);

        if (method_exists($this, $method)) {
            $this->$method($payload);

        } else {
            return response('There is no method');
        }
    }

    public function whenChargeSucceeded($payload)
    {

        // find the user by their stripe id,
        // and then..
        $this->retrieveUser($payload)
            ->payments()
            ->create([
                'amount' => $payload['data']['object']['amount'],
                'charge_id' => $payload['data']['object']['id']
            ]);
    }

    public function whenCustomerSubscriptionDeleted($payload)
    {
        $this->retrieveUser($payload)->deactivate();
    }

    public function whenCustomerSubscriptionCreated($payload)
    {
        $this->retrieveUser($payload)->subscription();
    }

    public function eventToMethod($event)
    {
        // a_string_separated_by_underscores => AStringSeparatedByUnderscores
        return 'when' . studly_case(str_replace('.', '_', $event));
        // return 'whenCustomerSubscriptionDeleted';
    }

    protected function retrieveUser($payload)
    {
        return User::byStripeId(
            $payload['data']['object']['customer']
        );
    }
}
