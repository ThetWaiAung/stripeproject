<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App\User;
use App\Plan;
use App\Product;

class CheckoutController extends Controller
{
    public function charge(Request $request)
    {
        $product = Product::findOrFail(request('product'));
        $customer = Customer::create([
            'email' => $request->stripeEmail,
            'source' => $request->stripeToken
        ]);

        $charge = Charge::create([
            'customer' => $customer->id,
            'amount' => $product->price,
            'currency' => 'usd'
        ]);

        return 'All done';
    }

    public function subscribe_process(Request $request)
    {
        try {
            Stripe::setApiKey(env('STRIPE_SECRET'));

            $user = User::find(1);
            $user->newSubscription('main', 'basic')->create($request->stripeToken);

            return 'Subscription successful, you get the course!';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
}
