<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationForm;
use Exception;

class SubscriptionController extends Controller
{
    public function store(RegistrationForm $form)
    {
        try {
            $form->save();
            return [
                'status' => 'Success!'
            ];
        } catch (Exception $e) {
            $body = $e->getJsonBody();
            $err  = $body['error']['message'];
            return response()->json(['status' => $err], 422);
        }
    }

    public function update()
    {
        if (request('resume')) {
            auth()->user()->subscription()->resume();
        }
        return back();
    }

    public function destroy()
    {
        //Cancel the user's subscription
        auth()->user()->subscription()->cancel();

        //And then redirect back.
        return back();
    }
}
