<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Plan;

class RegistrationForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !!$this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stripeEmail' => 'required|email',
            'stripeToken' => 'required',
            'plan' => 'required|max:25'
        ];
    }

    public function save()
    {
        $plan = Plan::findOrFail($this->plan);
        $this->user()
            ->subscription()
            ->usingCoupon($this->coupon)
            ->create($plan, $this->stripeToken);
    }
}
