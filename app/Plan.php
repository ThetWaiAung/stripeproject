<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    // protected $fillable = [
    //     'name',
    //     'slug',
    //     'stripe_plan',
    //     'price',
    //     'description'
    // ];
    protected $casts = [
        'price'
    ];
    protected $guarded = [];
    public function getRouteKeyName()
    {
        return 'stripe_plan';
    }
}
