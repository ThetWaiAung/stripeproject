<?php

namespace App;

use Carbon\Carbon;
use Stripe\Customer;
use Stripe\Charge;
use Stripe\Subscription as StripeSubscription;

class Subscription
{
    protected $user;
    protected $coupon;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function usingCoupon($coupon)
    {
        if ($coupon) {
            $this->coupon = $coupon;
        }
        return $this;
    }
    public function create(Plan $plan, $token)
    {
        $customer = Customer::create([
            'email' => $this->user->email,
            'source' => $token,
            'plan' => $plan->name,
            'coupon' => $this->coupon
        ]);
        // $invoices = \Stripe\Invoice::all(["limit" => 3]);
        // $invoiceId = $invoices->data[0]->id;
        // $chargeId = $invoices->data[0]->charge;
        // $charge = Charge::create([
        //     'customer' => $customer->id,
        //     'amount' => $plan->price,
        //     'currency' => 'usd'
        // ]);
        $subscriptionId = $customer->subscriptions->data[0]->id;
        //update the user in your database

        //Add column for stripe id
        //Redirect to success page
        $this->user->activate($customer->id, $subscriptionId);
    }

    public function cancel($atPeriodEnd = true)
    {
        $customer = $this->retrieveStripeCustomer();
        // $customer->cancelSubscription();
        // dd($customer);
        $subscriptionId = $customer->subscriptions->data[0]->id;

        $subCurrentEnd = StripeSubscription::update(
            $subscriptionId,
            [
              'cancel_at_period_end' => $atPeriodEnd,
            ]
        );

        // Update our user to reflect the cancellation.
        $endDate = Carbon::createFromTimestamp($subCurrentEnd->current_period_end);
        $this->user->deactivate($endDate);
    }

    public function cancelImmediately()
    {
        return $this->cancel(false);
    }

    public function resume()
    {
        $subscription = $this->retrieveStripeSubscription();
        $subscription->plan = 'monthly';
        $subscription->save();
        $this->user->activate();
        //replay the plan
        //save the Stripe subscription.
    }
    /**
     * Retrieve a user's Stripe-specific customer instance.
     *
     * @return \Stripe\Customer
     */
    public function retrieveStripeCustomer()
    {
        return Customer::retrieve($this->user->stripe_id);
    }

    public function retrieveStripeSubscription()
    {
        return StripeSubscription::retrieve($this->user->stripe_subscription);
    }
}
