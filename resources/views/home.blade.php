@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(auth()->user()->isOnGracePeriod())
                <div class="card">
                    <div class="card-header">Grace Period</div>

                    <div class="card-body">
                        <p>
                           Your subscription will fully expire on {{ auth()->user()->subscription_end_at }}
                        </p>
                        <form action="{{route('subscription.update',[auth()->user()->id])}}" method="POST">
                            @csrf
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="resume"/>Resume My Subscription
                                </label>
                            </div>
                            <button class="btn btn-primary" type="submit">Update</button>
                        </form>
                    </div>
                </div>
            @elseif(!auth()->user()->isSubscribed())

                <div class="card">
                    <div class="card-header">Create a Subscription</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <checkout-form :plans="{{ $plans }}"></checkout-form>
                    </div>
                </div>
            @endif
            @if(auth()->user()->payments)

                <div class="card mt-3">
                    <div class="card-header">Payments</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <ul class="list-group">
                            @foreach(auth()->user()->payments as $payment)
                            <li class="list-group-item">
                                {{ $payment->created_at->diffForHumans() }} :
                                    <strong>$ {{ $payment->inDollars() }} </strong>
                                    @if(auth()->user()->isSubscribed())
                                        <form action="{{ route('subscription.delete')}}" method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                        &nbsp;<span><button class="btn btn-dark">Cancel My Subscription</button></span>
                                        </form>
                                    @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

